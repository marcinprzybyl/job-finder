const express = require("express");
const paginate = require('express-paginate');
var mongoose = require('mongoose');

// 1. Create an Express Router
const router = express.Router();
var Job = mongoose.model('Job');
var Company = mongoose.model('Company');

router.get('/jobs/:id', function (req, res, next) {
    Job.findById(req.params.id).populate('company').then(function (job) {
    return res.json(job);
  }).catch(next);
});

router.get('/companies/:id/logo', function (req, res, next) {
  Company.findById(req.params.id,function (err, company) {
    if (err) return next(err);
    if(company.logo && company.logo.type) {
      var base64Data = company.logo.data.replace(/^data:image\/(png|jpeg|jpg);base64,/, '');
      var img = Buffer.from(base64Data, 'base64');
      res.writeHead(200, {
        'Content-Type': company.logo.type,
        'Content-Length': img.length
      });
      res.end(img);
    } else{
      res.status(404).send('');
    }
  });
});

router.get('/jobs', function (req, res, next) {
  var query = {};
  var limit = 20;
  var skip = 0 ;

  if(typeof req.query.search !== 'undefined') {
    query.name = { "$regex": req.query.search, "$options": "i" };
  }

  if (typeof req.query.limit !== 'undefined') {
    limit = req.query.limit;
  }

  if (typeof req.query.page !== 'undefined') {
    skip = (req.query.page - 1) * req.query.limit;
  }

  return Promise.all([
    Job.find(query)
      .populate('company')
      .limit(Number(limit))
      .skip(Number(skip))
      .sort({createdAt: 'desc'})
      .exec(),
    Job.count(query).exec()])
    .then((results) => {
      var jobs = results[0];
      const pageCount = Math.ceil(results[1] / req.query.limit);


      return res.json({
        data: jobs,
        pagination: {
          currentPage: skip/limit + 1,
          pages: paginate.getArrayPages(req)(limit, pageCount, req.query.page),
          total: results[1]
        }
      });
    }).catch(next);
});


router.post('/jobs', function (req, res, next) {
  Company.create(req.body.company).then((company) => {
    req.body.company = company._id;
    Job.create(req.body).then(function (job) {
      return res.json(job);
    }).catch(next);
  });


});



module.exports = router;
