const express = require("express");
const paginate = require('express-paginate');
var mongoose = require('mongoose');

// 1. Create an Express Router
const router = express.Router();
var User = mongoose.model('User');

router.get('/users/:id', function (req, res, next) {
    User.findById(req.params.id).then(function (user) {
        return res.json(user);
    }).catch(next);
});

router.put('/users/:id', function (req, res, next) {
    User.findOneAndUpdate({'_id': req.params.id}, req.body).then(function (user) {
        return res.json(user);
    }).catch(next);
});


module.exports = router;
