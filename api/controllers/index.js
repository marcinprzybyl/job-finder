var router = require('express').Router();

router.use('/', require('./authController'));
router.use('/api', require('./jobController'));
router.use('/api', require('./userController'));

module.exports = router;
