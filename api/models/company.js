const mongoose = require('mongoose');

const companyModel = mongoose.Schema({
  name: {
    type: mongoose.Schema.Types.String,
    required: '{PATH} is required!'
  },
  description: {
    type: mongoose.Schema.Types.String
  },
  logo: {
    data:{
      type: mongoose.Schema.Types.String
    },
    type:{
      type: mongoose.Schema.Types.String
    }
  }
}, {
  timestamps: true
});

module.exports = mongoose.model('Company', companyModel);
