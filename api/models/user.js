const mongoose = require('mongoose');

const userModel = mongoose.Schema({
    _id: {
        type: mongoose.Schema.ObjectId,
        auto: true
    },
    email: {
        type: mongoose.Schema.Types.String,
        required: '{PATH} is required!'
    },
    firstname: {
        type: mongoose.Schema.Types.String,
    },
    lastname: {
        type: mongoose.Schema.Types.String,
    },
    city: {
        type: mongoose.Schema.Types.String
    },
    phone: {
        type: mongoose.Schema.Types.String
    },
    picture: {
        type: mongoose.Schema.Types.String
    },
    completedRegistration: {
        type: mongoose.Schema.Types.Bool,
        default: false
    },
    company: {type: mongoose.Schema.Types.ObjectId, ref: 'Company'}
}, {
    timestamps: true
});

module.exports = mongoose.model('User', userModel);
