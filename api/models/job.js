const mongoose = require('mongoose');

const jobModel = mongoose.Schema({
  name: {
    type: mongoose.Schema.Types.String,
    required: '{PATH} is required!'
  },
  city: {
    type: mongoose.Schema.Types.String
  },
  salary: {
    type: mongoose.Schema.Types.Number
  },
  workTimeType: {
    type: mongoose.Schema.Types.String,
    enum : ['FULL','HALF'],
    default: 'FULL'
  },
  type: {
    type: mongoose.Schema.Types.String,
    enum : ['B2B','EMPLOYMENT'],
    default: 'EMPLOYMENT'
  },
  description: {
    type: mongoose.Schema.Types.String
  },
  company: {type: mongoose.Schema.Types.ObjectId, ref: 'Company'}
}, {
  timestamps: true
});

module.exports = mongoose.model('Job', jobModel);
