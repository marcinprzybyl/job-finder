import Router from 'next/router'

export const auth = ctx => {

    const userStore = ctx.store.userStore;

    if (ctx.req && !userStore.logged) {
        ctx.res.writeHead(302, { Location: '/login' });
        ctx.res.end();
        return
    }

    if (!userStore.logged) {
        Router.push('/login')
    }

    return userStore.getUserById(userStore.currentUserId);
};


// Gets the display name of a JSX component for dev tools
const getDisplayName = Component =>
    Component.displayName || Component.name || 'Component';

export const withAuthSync = WrappedComponent =>
    class extends React.Component {
        static displayName = `withAuthSync(${getDisplayName(WrappedComponent)})`

        static async getInitialProps (ctx) {
            const user = auth(ctx);

            const componentProps =
                WrappedComponent.getInitialProps &&
                (await WrappedComponent.getInitialProps(ctx));

            return { ...componentProps, user }
        }

        render () {
            return <WrappedComponent {...this.props} />
        }
    };
