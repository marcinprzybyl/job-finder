// stores/job.js

import {BaseStore, getOrCreateStore} from 'next-mobx-wrapper';
import {observable, action, flow, computed} from 'mobx';
import fetch from 'isomorphic-unfetch';
import axios from "axios";

class Store extends BaseStore {
  @observable loading = true;
  @observable jobs = [];
  @observable pagination = {
    currentPage: 1,
    pageSize: 10,
    total: 0,
    pages: []
  };


  @action
  setJobs(jobs){
    this.jobs = jobs.data;
  }

  @action
  setPagination(pagination){
    this.pagination = pagination;
  }

  @action
  setLoading(value){
    this.loading = value;
  }

  @action
  async fetchJobs(page, search) {
    try {
      this.setLoading(true);
      let url =  new URL(process.env.BASE_URL  + `/api/jobs/`);
      var query_string = url.search;
      var search_params = new URLSearchParams(query_string);

      if(page) {
        search_params.set('page', page);
      }

      if(search) {
        search_params.set('search', search);
      }

      url.search = search_params.toString();

      const jobs = await fetch(url.toString())
        .then(response => response.json());

      this.setJobs(jobs);
      this.setPagination({
        currentPage: jobs.pagination.currentPage,
        pageSize: this.pagination.pageSize,
        total: jobs.pagination.total,
        pages: jobs.pagination.pages
      });

      this.setLoading(false);

    } catch (error) {
      throw error;
    }
  };

  @action
  createJob(newJob) {
    return axios.post(process.env.BASE_URL +  `/api/jobs/`,newJob);
  }


  @action
  async fetchJob(id) {
    try {
      const job = await fetch(process.env.BASE_URL + `/api/jobs/${id}`)
        .then(response => response.json());

      var foundIndex = this.jobs.findIndex(x => x._id === job._id);
      if(foundIndex > -1){
        this.jobs[foundIndex] = item;
      } else{
        this.jobs.push(job);
      }

    } catch (error) {
      throw error;
    }
  };

  getJobById = id => {
    return this.jobs.find(x => x._id === id); //
  };
}

// Make sure the store’s unique name
// AND must be same formula
// Example: getUserStore => userStore
// Example: getProductStore => productStore
export const getJobStore = getOrCreateStore('jobStore', Store);
