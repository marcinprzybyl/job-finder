// stores/user.js

import {BaseStore, getOrCreateStore} from 'next-mobx-wrapper';
import {observable, action, flow} from 'mobx';
import fetch from 'isomorphic-unfetch';

class Store extends BaseStore {
    @observable userRegistry = new Map();
    @observable currentUserId = '';
    @observable logged = false;

    @action
    async setCurrentUser(user) {
        this.currentUserId = user.user_id.split('|').pop();
        this.logged = !!this.currentUserId;
        await this.fetchUser(this.currentUserId);
    }

    @action
    async fetchUser(id) {
        if (this.userRegistry.has(id)) {
            return;
        }

        try {
            const user = await fetch(process.env.BASE_URL + `/api/users/${id}`)
                .then(response => response.json());

            this.userRegistry.set(id, user);
        } catch (error) {
            throw error;
        }
    };

    getCurrentUser = () => {
        return this.userRegistry.get(this.currentUserId);
    };

    getUserById = id => {
        return this.userRegistry.get(id);
    };
}

// Make sure the store’s unique name
// AND must be same formula
// Example: getUserStore => userStore
// Example: getProductStore => productStore
export const getUserStore = getOrCreateStore('userStore', Store);
