webpackHotUpdate("static/development/pages/job.js",{

/***/ "./pages/job/index.jsx":
/*!*****************************!*\
  !*** ./pages/job/index.jsx ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var antd_lib_list_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! antd/lib/list/style */ "./node_modules/antd/lib/list/style/index.js");
/* harmony import */ var antd_lib_list_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(antd_lib_list_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var antd_lib_list__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! antd/lib/list */ "./node_modules/antd/lib/list/index.js");
/* harmony import */ var antd_lib_list__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(antd_lib_list__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var antd_lib_divider_style__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! antd/lib/divider/style */ "./node_modules/antd/lib/divider/style/index.js");
/* harmony import */ var antd_lib_divider_style__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(antd_lib_divider_style__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var antd_lib_divider__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! antd/lib/divider */ "./node_modules/antd/lib/divider/index.js");
/* harmony import */ var antd_lib_divider__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(antd_lib_divider__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var antd_lib_card_style__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! antd/lib/card/style */ "./node_modules/antd/lib/card/style/index.js");
/* harmony import */ var antd_lib_card_style__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(antd_lib_card_style__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var antd_lib_card__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! antd/lib/card */ "./node_modules/antd/lib/card/index.js");
/* harmony import */ var antd_lib_card__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(antd_lib_card__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @babel/runtime/helpers/esm/classCallCheck */ "./node_modules/@babel/runtime/helpers/esm/classCallCheck.js");
/* harmony import */ var _babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @babel/runtime/helpers/esm/createClass */ "./node_modules/@babel/runtime/helpers/esm/createClass.js");
/* harmony import */ var _babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @babel/runtime/helpers/esm/possibleConstructorReturn */ "./node_modules/@babel/runtime/helpers/esm/possibleConstructorReturn.js");
/* harmony import */ var _babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @babel/runtime/helpers/esm/getPrototypeOf */ "./node_modules/@babel/runtime/helpers/esm/getPrototypeOf.js");
/* harmony import */ var _babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @babel/runtime/helpers/esm/inherits */ "./node_modules/@babel/runtime/helpers/esm/inherits.js");
/* harmony import */ var antd_lib_icon_style__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! antd/lib/icon/style */ "./node_modules/antd/lib/icon/style/index.js");
/* harmony import */ var antd_lib_icon_style__WEBPACK_IMPORTED_MODULE_12___default = /*#__PURE__*/__webpack_require__.n(antd_lib_icon_style__WEBPACK_IMPORTED_MODULE_12__);
/* harmony import */ var antd_lib_icon__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! antd/lib/icon */ "./node_modules/antd/lib/icon/index.js");
/* harmony import */ var antd_lib_icon__WEBPACK_IMPORTED_MODULE_13___default = /*#__PURE__*/__webpack_require__.n(antd_lib_icon__WEBPACK_IMPORTED_MODULE_13__);
/* harmony import */ var antd_lib_input_style__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! antd/lib/input/style */ "./node_modules/antd/lib/input/style/index.js");
/* harmony import */ var antd_lib_input_style__WEBPACK_IMPORTED_MODULE_14___default = /*#__PURE__*/__webpack_require__.n(antd_lib_input_style__WEBPACK_IMPORTED_MODULE_14__);
/* harmony import */ var antd_lib_input__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! antd/lib/input */ "./node_modules/antd/lib/input/index.js");
/* harmony import */ var antd_lib_input__WEBPACK_IMPORTED_MODULE_15___default = /*#__PURE__*/__webpack_require__.n(antd_lib_input__WEBPACK_IMPORTED_MODULE_15__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_16___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_16__);
/* harmony import */ var mobx_react__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! mobx-react */ "./node_modules/mobx-react/dist/mobx-react.module.js");

















var _dec, _class;


var __jsx = react__WEBPACK_IMPORTED_MODULE_16___default.a.createElement;

var Search = antd_lib_input__WEBPACK_IMPORTED_MODULE_15___default.a.Search;

var IconText = function IconText(type, text) {
  return __jsx("span", null, __jsx(antd_lib_icon__WEBPACK_IMPORTED_MODULE_13___default.a, {
    type: type,
    theme: "twoTone"
  }), " ", text);
};

var Jobs = (_dec = Object(mobx_react__WEBPACK_IMPORTED_MODULE_17__["inject"])('jobStore'), _dec(_class = Object(mobx_react__WEBPACK_IMPORTED_MODULE_17__["observer"])(_class =
/*#__PURE__*/
function (_React$Component) {
  Object(_babel_runtime_helpers_esm_inherits__WEBPACK_IMPORTED_MODULE_11__["default"])(Jobs, _React$Component);

  function Jobs() {
    Object(_babel_runtime_helpers_esm_classCallCheck__WEBPACK_IMPORTED_MODULE_7__["default"])(this, Jobs);

    return Object(_babel_runtime_helpers_esm_possibleConstructorReturn__WEBPACK_IMPORTED_MODULE_9__["default"])(this, Object(_babel_runtime_helpers_esm_getPrototypeOf__WEBPACK_IMPORTED_MODULE_10__["default"])(Jobs).apply(this, arguments));
  }

  Object(_babel_runtime_helpers_esm_createClass__WEBPACK_IMPORTED_MODULE_8__["default"])(Jobs, [{
    key: "render",
    value: function render() {
      var jobStore = this.props.jobStore;
      return __jsx("div", null, __jsx(antd_lib_card__WEBPACK_IMPORTED_MODULE_5___default.a, {
        bordered: false
      }, __jsx(Search, {
        placeholder: "input search text",
        enterButton: "Search",
        size: "large",
        onSearch: function _callee(value) {
          return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.async(function _callee$(_context) {
            while (1) {
              switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.awrap(jobStore.fetchJobs(1, value));

                case 2:
                  return _context.abrupt("return", _context.sent);

                case 3:
                case "end":
                  return _context.stop();
              }
            }
          });
        }
      })), __jsx(antd_lib_divider__WEBPACK_IMPORTED_MODULE_3___default.a, {
        type: "vertical"
      }), __jsx(antd_lib_card__WEBPACK_IMPORTED_MODULE_5___default.a, {
        bordered: false
      }, __jsx(antd_lib_list__WEBPACK_IMPORTED_MODULE_1___default.a, {
        itemLayout: "vertical",
        size: "large",
        loading: jobStore.loading,
        pagination: {
          onChange: function onChange(page) {
            return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.async(function onChange$(_context2) {
              while (1) {
                switch (_context2.prev = _context2.next) {
                  case 0:
                    _context2.next = 2;
                    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.awrap(jobStore.fetchJobs(page));

                  case 2:
                  case "end":
                    return _context2.stop();
                }
              }
            });
          },
          current: jobStore.pagination.currentPage,
          pageSize: jobStore.pagination.pageSize,
          total: jobStore.pagination.total
        },
        dataSource: jobStore.jobs,
        renderItem: function renderItem(item) {
          return __jsx(antd_lib_list__WEBPACK_IMPORTED_MODULE_1___default.a.Item, {
            key: item._id,
            actions: [IconText("pushpin", item.city), IconText("dollar", item.salary + " zł"), IconText("clock-circle", item.workTimeType), IconText("file-text", item.type)],
            extra: item.company && item.company.logo && __jsx("img", {
              width: 272,
              alt: "logo",
              src: item.company && "/api/companies/".concat(item.company._id, "/logo")
            })
          }, __jsx(antd_lib_list__WEBPACK_IMPORTED_MODULE_1___default.a.Item.Meta, {
            title: __jsx("a", {
              href: '/job/' + item._id
            }, item.name),
            description: item.company && __jsx("div", null, __jsx("a", {
              href: '/company/' + item.company._id
            }, item.company.name))
          }));
        }
      })));
    }
  }], [{
    key: "getInitialProps",
    value: function getInitialProps(_ref) {
      var jobStore, query, pageProps;
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.async(function getInitialProps$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              jobStore = _ref.store.jobStore, query = _ref.query;
              pageProps = {};
              _context3.next = 4;
              return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_6___default.a.awrap(jobStore.fetchJobs());

            case 4:
              return _context3.abrupt("return", pageProps);

            case 5:
            case "end":
              return _context3.stop();
          }
        }
      });
    }
  }]);

  return Jobs;
}(react__WEBPACK_IMPORTED_MODULE_16___default.a.Component)) || _class) || _class);
/* harmony default export */ __webpack_exports__["default"] = (Jobs);

/***/ })

})
//# sourceMappingURL=job.js.98616523e150558d1c08.hot-update.js.map