import {Component, useState} from 'react'
import dynamic from 'next/dynamic'
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import CKEditor from '@ckeditor/ckeditor5-react';

class CKEditorWrapper extends Component {
    render() {
        return <div>
            <CKEditor
                editor={ ClassicEditor }
                {...this.props}
            />
        </div>
    }
}


const Editor = ({value = {}, onChange}) => {
    const [description, setDescription] = useState("");

    const triggerChange = changedValue => {
        if (onChange) {
            onChange(description);
        }
    };

    const onTextChange = (event, editor ) => {
        const newValue = editor.getData();

        setDescription(newValue);
        triggerChange({ description: newValue});
    };

    return (<CKEditorWrapper onChange={onTextChange}/>);
};

export default Editor;