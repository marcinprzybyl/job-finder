import {Modal, Button, Card, Upload, Icon} from 'antd';

const { Dragger } = Upload;

const props = {
  name: 'file',
  multiple: false,
  action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
  onChange(info) {

  },
  beforeUpload(file) {
    return false;
  }
};


class JobApplicationModal extends React.Component {
  state = { visible: false };

  showModal = () => {
    if(!this.props.beforeOpen || this.props.beforeOpen() !== false){
      this.setState({
        visible: true,
      });
    }
  };

  handleOk = e => {
    alert("Tutaj będzie się wysyłać CV, TODO// Dodać CV do profilu jak powstanie");
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false,
    });
  };

  render() {
    return (
      <div>
        <Button type="primary" shape="round" icon="select" size="large" block onClick={this.showModal}>
          Aplikuj teraz
        </Button>
        <Modal
          title="CV"
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Dragger {...props}>
            <p className="ant-upload-drag-icon">
              <Icon type="inbox" />
            </p>
            <p className="ant-upload-text">Kliknij i wybierz lub upuść plik ze swoim CV w to miejsce</p>
            <p className="ant-upload-hint">

            </p>
          </Dragger>
        </Modal>
      </div>
    );
  }
}

export default JobApplicationModal;
