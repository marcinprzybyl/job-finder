import {Form, Input, Button, Checkbox, Card} from 'antd';

class Login extends React.Component {
  static async getInitialProps({store: {userStore}, query}) {
    const {id} = query;
    let pageProps = {};


    return pageProps;
  }


  render() {

    return (
      <Card bordered={false} style={{maxWidth:'400px',margin:'auto'}}>
        <Form
          name="normal_login"
          className="login-form"
          initialValues={{remember: true}}
        >
          <Form.Item
            name="username"
            rules={[{required: true, message: 'Please input your Username!'}]}
          >
            <Input placeholder="Username"/>
          </Form.Item>
          <Form.Item
            name="password"
            rules={[{required: true, message: 'Please input your Password!'}]}
          >
            <Input
              type="password"
              placeholder="Password"
            />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Log in
            </Button>
            Or <a href="">register now!</a>
          </Form.Item>
        </Form>
      </Card>);
  }
}

export default Login;

