import {
    Form,
    Input,
    Tooltip,
    Icon,
    Cascader,
    Select,
    Row,
    Col,
    Checkbox,
    Button,
    AutoComplete,
    Card,
    Upload,
    Avatar, message
} from 'antd';
import axios from 'axios'
import {withAuthSync} from "../../../utils/auth";
import Router from "next/router";

const {Option} = Select;
const AutoCompleteOption = AutoComplete.Option;


class ProfileForm extends React.Component {
    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    static async getInitialProps({store: {userStore}}){
        let pageProps = {};
        pageProps.userEntity = userStore.getUserById(userStore.currentUserId);

        return pageProps;
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                const newUser = {
                  ...this.props.userEntity, ...values
                };

                // TODO move to store
                axios.put(process.env.BASE_URL +  `/api/users/${newUser._id}`,newUser).then(() => {
                    message.success('Profile updated.');
                    Router.push('/');
                });
            }
        });
    };s

    render() {
        const {getFieldDecorator} = this.props.form;
        const {userEntity} = this.props;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        return userEntity && (
            <Card bordered={false}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>

                    <Row gutter={16}>
                        <Col span={6}>
                            <Form.Item label="E-mail">
                                {getFieldDecorator('email', {
                                    initialValue: userEntity.email,
                                    rules: [
                                        {
                                            type: 'email',
                                            message: 'The input is not valid E-mail!',
                                        },
                                        {
                                            required: true,
                                            message: 'Please input your E-mail!',
                                        },
                                    ],
                                })(<Input />)}
                            </Form.Item>

                            <Form.Item label="Phone">
                                {getFieldDecorator('phone', {
                                    initialValue: userEntity.phone,
                                    rules: [{required: true, message: 'Please input your phone number!'}],
                                })(<Input style={{width: '100%'}}/>)}
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Firstname">
                                {getFieldDecorator('firstname', {
                                    initialValue: userEntity.firstname,
                                })(<Input />)}
                            </Form.Item>

                            <Form.Item label="City">
                                {getFieldDecorator('city', {
                                    initialValue: userEntity.city,
                                })(<Input />)}
                            </Form.Item>
                        </Col>
                        <Col span={6}>
                            <Form.Item label="Lastname">
                                {getFieldDecorator('lastname', {
                                    initialValue: userEntity.lastname,
                                })(<Input />)}
                            </Form.Item>
                        </Col>
                        <Col span={6} style={{textAlign:'center'}}>
                                <Avatar size={250} src={userEntity.picture} alt="avatar" icon="user">
                                </Avatar>
                        </Col>
                    </Row>
                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Save
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        );
    }
}

const WrappedProfileForm = Form.create({name: 'profile'})(ProfileForm);
export default withAuthSync(WrappedProfileForm);

