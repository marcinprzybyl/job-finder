import {List, Avatar, Icon, Input, Row, Col, Card, Divider} from 'antd';
import {inject, observer} from "mobx-react";

const {Search} = Input;

const IconText = (type, text) => (
  <span>
    <Icon type={type} theme="twoTone"/> {text}
  </span>
);

@inject('jobStore') @observer
class Jobs extends React.Component {
  static async getInitialProps({store: {jobStore}, query}) {
    let pageProps = {};

    await jobStore.fetchJobs();

    return pageProps;
  }


  render() {
    const {jobStore} = this.props;

    return (<div>
        <Card bordered={false}>
          <Search
            placeholder="input search text"
            enterButton="Search"
            size="large"
            onSearch={async value => await jobStore.fetchJobs(1,value)}
          />
        </Card>
        <Divider type="vertical"/>
        <Card bordered={false}>
          <List
            itemLayout="vertical"
            size="large"
            loading={jobStore.loading}
            pagination={{
              onChange: async page => {
                  await jobStore.fetchJobs(page);
              },
                current: jobStore.pagination.currentPage,
                pageSize: jobStore.pagination.pageSize,
                total: jobStore.pagination.total,
            }}
            dataSource={jobStore.jobs}
            renderItem={item => (
              <List.Item
                key={item._id}
                actions={[
                  IconText("pushpin", item.city),
                  IconText("dollar", item.salary + " zł"),
                  IconText("clock-circle", item.workTimeType),
                  IconText("file-text", item.type)
                ]}
                extra={item.company && item.company.logo && (
                  <img
                    width={272}
                    alt="logo"
                    src={item.company && `/api/companies/${item.company._id}/logo`}
                  />)
                }
              >
                <List.Item.Meta
                  title={<a href={'/job/' + item._id}>{item.name}</a>}
                  description={(item.company && (<div>
                    <a href={'/company/' + item.company._id}>{item.company.name}</a>
                  </div>
                  ))}
                />
              </List.Item>
            )}
          />
        </Card>
      </div>
    );
  }
}

export default Jobs;

