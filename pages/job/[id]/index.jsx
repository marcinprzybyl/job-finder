import {Button, Card, Col, Divider, Icon, List, Row, Statistic, Typography} from "antd";
import Router from 'next/router'
import JobApplicationModal from "../../../components/jobApplicationModal";
const { Title } = Typography;

class Job extends React.Component {

  static async getInitialProps({store: {jobStore}, query, req}){
    const {id} = query;
    let pageProps = {};

    // TODO Move it to user store
    if (req && req.session.passport) {
      pageProps.user = req.session.passport.user;
    }

    await jobStore.fetchJob(id);
    pageProps.job = jobStore.getJobById(id);

    return pageProps;
  }


  render() {
    const {job, user} = this.props;

    return (<Row gutter={25}>
        <Col span={18}>
          <Card bordered={false}>
            <Row gutter={25}>
              <Col span={6}>
                <Statistic
                    title="Salary"
                    value={job.salary}
                    precision={2}
                    valueStyle={{ color: '#1890ff' }}
                    prefix={<Icon type="dollar" />}
                    suffix="zł"
                />
              </Col>

              <Col span={6}>
                <Statistic
                    title="City"
                    value={job.city}
                    valueStyle={{ color: '#1890ff' }}
                    prefix={<Icon type="pushpin" />}
                />
              </Col>

              <Col span={6}>
                <Statistic
                    title="Time"
                    value={job.workTimeType}
                    valueStyle={{ color: '#1890ff' }}
                    prefix={<Icon type="clock-circle" />}
                />
              </Col>

              <Col span={6}>
                <Statistic
                    title="Type"
                    value={job.type}
                    valueStyle={{ color: '#1890ff' }}
                    prefix={<Icon type="file-text" />}
                />
              </Col>
            </Row>
          </Card>
          <Card bordered={false} style={{minHeight:"600px"}}>
            <Title>{job.name}</Title>
            <div style={{margin:'auto'}} dangerouslySetInnerHTML={{ __html: job.description}} />
          </Card>
        </Col>
        <Col span={6}>
          <Card bordered={false} style={{marginBottom:"25px"}} >
            <JobApplicationModal beforeOpen ={() => {
              if(!user){
                //TODO add callback
                Router.push('/login');
              }

              return !!user
            } } />
          </Card>
          {job.company && (
              <Card bordered={false} cover={<img alt="example" src={job.company && `/api/companies/${job.company._id}/logo`} />}
              >
                <Title level={2}>{job.company.name}</Title>
                <p  dangerouslySetInnerHTML={{ __html: job.company.description}}></p>
              </Card>
          )}
        </Col>
      </Row>
    );
  }
}

export default Job;

