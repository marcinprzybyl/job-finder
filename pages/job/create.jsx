import {Button, Card, Form, Icon, Input, InputNumber, Select, Typography, Upload} from 'antd';
const { Title } = Typography;
const { Option } = Select;
import { message } from 'antd';

import dynamic from 'next/dynamic'
import {inject, observer} from "mobx-react";
import Router from "next/router";

const CKEditor = dynamic(() => import('../../components/CKEditor'), {
    ssr: false
});

@inject('jobStore') @observer
class CreateJob extends React.Component {
    static async getInitialProps({store: {jobStore}, query}) {
        return {};
    }

    state = {
        confirmDirty: false,
        autoCompleteResult: [],
    };

    handleSubmit = e => {
        e.preventDefault();
        const {form, jobStore} = this.props;
        form.validateFieldsAndScroll((err, values) => {
            const reader = new FileReader();

            reader.onload = e => {
                if (!err) {
                    values.company.logo = {
                        type: values.company.logo.file.type,
                        data: e.target.result
                    };

                    jobStore.createJob(values).then(() => {
                        message.success('Job offer added.');
                        Router.push('/');
                    });
                }
            };
            reader.readAsDataURL(values.company.logo.file);
        });
    };

    handleConfirmBlur = e => {
        const {value} = e.target;
        this.setState({confirmDirty: this.state.confirmDirty || !!value});
    };


    render() {
        const {getFieldDecorator} = this.props.form;
        const {autoCompleteResult} = this.state;

        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 8},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 16,
                    offset: 8,
                },
            },
        };

        const websiteOptions = autoCompleteResult.map(website => (
            <AutoCompleteOption key={website}>{website}</AutoCompleteOption>
        ));

        return (
            <Card bordered={false}>
                <Form {...formItemLayout} onSubmit={this.handleSubmit}>
                    <Title>Job offer</Title>
                    <Form.Item label="Name">
                        {getFieldDecorator('name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input job name!',
                                },
                            ],
                        })(<Input/>)}
                    </Form.Item>
                    <Form.Item label="City">
                        {getFieldDecorator('city', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input city name!',
                                },
                            ],
                        })(<Input/>)}
                    </Form.Item>

                    <Form.Item label="Time">
                        {getFieldDecorator('workTimeType', { initialValue: 'FULL' })(
                            <Select>
                                <Option value="FULL">Full Time Work</Option>
                                <Option value="HALF">Half Time Work</Option>
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Type">
                        {getFieldDecorator('type', { initialValue: 'EMPLOYMENT'  })(
                            <Select>
                                <Option value="EMPLOYMENT">Employment contract</Option>
                                <Option value="B2B">B2B</Option>
                            </Select>
                        )}
                    </Form.Item>

                    <Form.Item label="Salary">
                        {getFieldDecorator('salary', {})
                        (<InputNumber />)}
                    </Form.Item>
                    <Form.Item label="Description">
                        {getFieldDecorator('description', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input your description!',
                                },
                            ],
                        })(<CKEditor/>)}
                    </Form.Item>

                    <Title>Company</Title>
                    <Form.Item label="Name">
                        {getFieldDecorator('company.name', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input company name!',
                                },
                            ],
                        })(<Input/>)}
                    </Form.Item>
                    <Form.Item label="Logo">
                        {getFieldDecorator('company.logo', {
                        })(
                            <Upload name="logo" listType="picture"
                            beforeUpload={() => {return false}}>
                                <Button>
                                    <Icon type="upload" /> Click to upload
                                </Button>
                            </Upload>,
                        )}
                    </Form.Item>

                    <Form.Item label="Description">
                        {getFieldDecorator('company.description', {
                            rules: [
                                {
                                    required: true,
                                    message: 'Please input description!',
                                },
                            ],
                        })(<CKEditor/>)}
                    </Form.Item>

                    <Form.Item {...tailFormItemLayout}>
                        <Button type="primary" htmlType="submit">
                            Save
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        );
    }
}

const WrappedJobForm = Form.create({name: 'job'})(CreateJob);
export default WrappedJobForm;

