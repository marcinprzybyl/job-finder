// pages/_app.jsx
import React from "react";
import App from "next/app";
import Head from "next/head";
import Link from 'next/link'

import '../assets/styles.less';

import {withMobx} from 'next-mobx-wrapper';
import {configure} from 'mobx';
import {Provider, useStaticRendering, observer} from 'mobx-react';

import * as getStores from '../stores';

const isServer = !process.browser;

configure({enforceActions: 'observed'});
useStaticRendering(isServer); // NOT `true` value

import {Layout, Menu, Divider} from 'antd';

const {Header, Footer, Content} = Layout;

@observer
class MyApp extends App {
    // This ensures that user details are passed to each page
    static async getInitialProps({Component, ctx}) {
        let pageProps = {};
        let user;

        if (ctx.req && ctx.req.session.passport && ctx.req.session.passport.user) {
            user = ctx.req.session.passport.user;
            await ctx.store.userStore.setCurrentUser(user);
        }

        if (Component.getInitialProps) {
            pageProps = await Component.getInitialProps(ctx);
            pageProps.user = user;
        }

        return {pageProps};
    }

    constructor(props) {
        super(props);
        this.state = {
            user: props.pageProps.user
        };
    }

    render() {
        const {Component, pageProps, store} = this.props;
        const props = {
            ...pageProps,
            user: this.state.user
        };
        return (
            <>
                <Provider {...store}>
                    <Head>
                        <title>JobFinder</title>
                    </Head>
                    <Layout style={{minHeight: "100vh"}}>
                        <Header theme='light'>
                            <a className="logo" href='/'></a>
                            <Menu
                                theme="light"
                                mode="horizontal"
                                style={{lineHeight: '64px'}}
                            >
                                {store.userStore.logged && (
                                    <Menu.Item key="1">
                                        <Link href="/account/profile">
                                            <a className="nav-link">Profile</a>
                                        </Link>
                                    </Menu.Item>
                                )}
                                {store.userStore.logged && (
                                    <Menu.Item key="2">
                                    <Link href="/job/create">
                                    <a className="nav-link">New offer</a>
                                    </Link>
                                    </Menu.Item>
                                )}

                                <Menu.Item key="3" style={{float: 'right'}}>
                                    {!store.userStore.logged && (
                                        <Link href="/login">
                                            <a>Log In</a>
                                        </Link>
                                    )}
                                    {store.userStore.logged  && (
                                        <Link href="/logout">
                                            <a className="nav-link">Log Out</a>
                                        </Link>
                                    )}
                                </Menu.Item>
                            </Menu>
                        </Header>
                        <Content style={{padding: "25px 50px"}}>
                            <div>
                                <Component {...pageProps} />
                            </div>
                        </Content>
                        <Footer style={{textAlign: 'center'}}>JobFinder ©2020 Created by Marcin Przybył</Footer>
                    </Layout>
                </Provider>
            </>
        );
    }
}

export default withMobx(getStores)(MyApp)
